<?php

namespace App\Console\Commands;

use App\Website;
use App\WebsiteRequest;
use App\WebsiteResponse;
use Illuminate\Console\Command;

class WebsiteChecker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websites:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh the status of all websites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (WebsiteRequest::all() as $w){
            $status = $w->sendRequest();
            $this->info($w->url. ' refreshed');
        }
    }
}
