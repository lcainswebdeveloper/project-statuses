<?php

namespace App\Http\Controllers;

use App\WebsiteResponse;
use Illuminate\Http\Request;

class WebsiteResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebsiteResponse  $websiteResponse
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteResponse $websiteResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebsiteResponse  $websiteResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteResponse $websiteResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebsiteResponse  $websiteResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteResponse $websiteResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebsiteResponse  $websiteResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteResponse $websiteResponse)
    {
        //
    }
}
