<?php
namespace App\Http;

class Slack {
    protected $client;
    public function __construct() {
        $this->client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
    }
    
    public function notify($content = []) {
        $response = $this->client->post(env('SLACK_URL'), [
            'body' => json_encode($content), 
        ]);

        return $response;
    }
}