import notify from './notify';
window.notify = notify;

import axios from './http';
window.Vue = require('vue');
Vue.prototype.$axios = new axios();


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context(
    // The relative path of the components folder
    './components',
    // Whether or not to look in subfolders
    true,
    /[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
    // Get component config
    const componentConfig = requireComponent(fileName)
    const filenameParsed = fileName.split('/');
    filenameParsed.shift();
    // Get PascalCase name of component
    const componentName = upperFirst(
            camelCase(
                // Gets the file name regardless of folder depth
                filenameParsed.join('').replace(/\.\w+$/, '')
            )
        )
        // Register component globally
    Vue.component(
        componentName,
        componentConfig.default || componentConfig
    )
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
