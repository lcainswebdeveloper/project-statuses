<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteRequest extends Model
{
    protected $fillable = [
      'website_id',
      'url',
      'request_type',
      'request_headers',
      'request_body',
      'expected_status',
      'latest_status_code'
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            $model->sendRequest();
        });
    }

    public function website() {
      return $this->belongsTo(Website::class);
    }
    
    public function responses() {
      return $this->hasMany(WebsiteResponse::class)->orderBy('created_at', 'desc');
    }

    public function postData() {
      if(in_array($this->request_type, ['POST', 'PUT'])){
        return [
          'body' => $this->request_body,
          'headers' => (array)json_decode($this->request_headers)
        ];
      }
      return [];
    }

    public function guzzleCall() {
      $client = new \GuzzleHttp\Client();
      if($this->request_type == 'GET'){
        \Log::info([
          'url' => $this->url,
          'request_type' => $this->request_type
        ]);
        return $client->get($this->url);
      } elseif ($this->request_type == 'POST'){
        \Log::info([
          'url' => $this->url,
          'request_type' => $this->request_type,
          'post_data' => $this->postData()
        ]);
        return $client->post($this->url, $this->postData());
      }
      \Log::info([
        'url' => $this->url,
        'request_type' => $this->request_type
      ]);
      return $client;
    }

    public function sendRequest() {
      $one = microtime(1);
      try{
          $response = $this->guzzleCall();
          $two = microtime(1);
          $response->getStatusCode();
          $this->latest_status_code = $response->getStatusCode();
          $this->save();
          WebsiteResponse::create([
              'website_request_id' => $this->id,
              //'response' => ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) ? json_encode([]) : json_encode($response->getBody()),
              'time' => ( $two - $one ),
              'status' => $response->getStatusCode(),
          ]);
          return $response->getStatusCode();
      } catch( \Exception $e){
          $this->latest_status_code = $e->getCode();
          $this->save();
          $two = microtime(1);
          
          WebsiteResponse::create([
              'website_request_id' => $this->id,
              'response' => json_encode($e->getMessage()),
              'time' => ( $two - $one ),
              'status' => $e->getCode(),
          ]);
          return $e->getCode();
      }
      
  }
}
