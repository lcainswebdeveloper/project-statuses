<?php

use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AppsTableSeeder::class);
        User::truncate();
        User::create([
            'name' => 'Admin',
            'email' => 'admin@tshirtandsons.co.uk',
            'password' => bcrypt('password')
        ]);
    }
}
