<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('website_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('website_id')->unsigned()->nullable();
            $table->longtext('url');
            $table->string('request_type')->default('GET');
            $table->json('request_headers')->nullable();
            $table->json('request_body')->nullable();
            $table->integer('expected_status')->default(200);
            $table->integer('latest_status_code')->nullable();
            $table->foreign('website_id')->references('id')->on('websites');
            $table->timestamps();
        });

        Schema::create('website_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('website_request_id')->unsigned()->nullable();
            $table->json('response')->nullable();
            $table->decimal('time', 11,6)->nullable();
            $table->integer('status')->nullable();
            $table->foreign('website_request_id')->references('id')->on('website_requests');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('websites');
        Schema::dropIfExists('website_responses');
        Schema::dropIfExists('website_requests');
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
