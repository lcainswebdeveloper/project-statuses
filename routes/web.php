<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');
Route::get('/websites', 'WebsiteController@index');
Route::get('/admin/websites', 'Admin\WebsiteController@index');
Route::get('/admin/websites/{website}', 'Admin\WebsiteController@show');
Route::get('/websites/{website}/refresh', 'WebsiteController@refresh');
Route::get('/websites/{website}', 'WebsiteController@show');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');
