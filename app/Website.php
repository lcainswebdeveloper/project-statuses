<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $fillable = [
        'name',
    ];

    public function requests() {
        return $this->hasMany(WebsiteRequest::class)->orderBy('created_at', 'desc');
    }

    public function requests_and_responses() {
        return $this->hasMany(WebsiteRequest::class)->orderBy('created_at', 'desc')->with('responses');
    }
}
