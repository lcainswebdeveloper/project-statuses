import alertify from 'alertifyjs';
alertify.defaults.glossary.title = 'TShirt & Sons';
alertify.defaults.notifier = {
    delay: 5,
    position: 'bottom-right',
    closeButton: true
};
require('alertifyjs/build/css/alertify.min.css');
require('alertifyjs/build/css/themes/default.min.css');

export default {
    alert(text, btnText = 'OK') {
        alertify
            .alert(text, function() {
                alertify.message(btnText);
            });
    },
    message(message) {
        alertify.message(message);
    },
    success(message) {
        alertify.success(message);
    },
    error(message) {
        alertify.error(message);
    },
    confirm(confirmMsg, config) {
        alertify.confirm(confirmMsg,
            function() {
                config.onConfirm();
            },
            function() {
                alertify.error('Cancelled');
            });
    }
}