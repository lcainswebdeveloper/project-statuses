<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/websites', 'Admin\WebsiteController@store')->middleware('auth:api');
Route::put('/websites/{website}', 'Admin\WebsiteController@update')->middleware('auth:api');
Route::post('/websites/{website}/requests', 'Admin\WebsiteRequestController@store')->middleware('auth:api');
Route::put('/websites/{website}/requests/{request}', 'Admin\WebsiteRequestController@update')->middleware('auth:api');
Route::get('/websites/{website}/requests/{request}', 'Admin\WebsiteRequestController@show')->middleware('auth:api');
Route::get('/websites/{website}/requests/{request}/refresh', 'Admin\WebsiteRequestController@refresh');
Route::get('hello', function() {
    return response()->json('You are calling a Laravel Project :-)');
});
