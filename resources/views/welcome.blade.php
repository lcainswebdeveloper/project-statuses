@extends('layouts.landing')

@section('content')
  <website-statuses :websites="{{$websites}}"></website-statuses>
@endsection