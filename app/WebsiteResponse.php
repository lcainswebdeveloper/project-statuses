<?php

namespace App;

use App\Http\Slack;
use Illuminate\Database\Eloquent\Model;

class WebsiteResponse extends Model
{
    protected $fillable = [
        'website_request_id',
        'response',
        'time',
        'status',
    ];

    public function request_with_website() {
        return $this->belongsTo(WebsiteRequest::class, 'website_request_id')->with('website');
    }

    public static function boot() {
        parent::boot();
        static::created(function($model){
            if ($model->status != $model->request_with_website->expected_status) {
                (new Slack())->notify([
                    "blocks" => [
                        [
                            "type" => "section",
                            "text" => [
                                "type" => "mrkdwn",
                                "text" => "<!channel> THE FOLLOWING WEBSITE HAS A ".$model->status." STATUS: ".$model->request_with_website->website->name
                            ]
                        ],
                        [
                            "type" => "section",
                            "text" => [
                                "type" => "mrkdwn",
                                "text" => "Issue is with this request: ".(string) $model->request_with_website->url
                            ]
                            ],
                            [
                                "type" => "section",
                                "text" => [
                                    "type" => "mrkdwn",
                                    "text" => "----------------------------------------------------------------------------"
                                ]
                            ],
                    ]
                ]);
            }
        });
    }
}
