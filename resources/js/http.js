import axios from 'axios';
export default class {
  constructor() {
    this.axios = axios;
    let token = document.head.querySelector('meta[name="csrf-token"]');
    this.axiosConfig = {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': token.content
      },
    }
  }

  setAxiosConfig(config) {
    const merged = Object.assign(config, this.axiosConfig);
    this.axiosConfig = merged;
  }

  setAuthBearerHeaderToken() {
    const token = api_token;
    this.axiosConfig.headers['Authorization'] = `Bearer ${token}`;
  }

  get(url) {
    return new Promise((res, rej) => {
      return this.axios.get(url, this.axiosConfig)
        .then((response) => {
          return res(response);
        })
        .catch((error) => {
          console.log(error);
          return rej(this.handleError(error));
        })
    });
  }

  post(url, payload) {
    return new Promise((res, rej) => {
      this.axios.post(url, payload, this.axiosConfig)
        .then(response => res(response)).catch((error) => {
        return rej(this.handleError(error));
      });
    })
  }

  put(url, payload) {
    return new Promise((res, rej) => {
      this.axios.put(url, payload, this.axiosConfig)
        .then(response => res(response)).catch((error) => {
          console.log('fired2');
          return rej(this.handleError(error));
      });
    })
    
  }

  delete(url) {
    return this.axios.delete(url, this.axiosConfig);
  }

  handleError(error) {
    let errorResponse = {
      status: error.response.status,
      message: error,
      validationErrors: {}
    };
    if (error.response) {
      if (error.response.status === 401) {
        errorResponse.status = error.response.status;
        errorResponse.message = "Invalid credentials were sent for retrieving your settings";

      } else if(error.response.status === 422) {
        errorResponse.validationErrors = error.response.data.errors;
        errorResponse.message = "Validation errors discovered";
      } else {
        errorResponse.message = error.response.data.message;
      }
    }
    notify.error(errorResponse.message);
    return errorResponse;
  }
}
