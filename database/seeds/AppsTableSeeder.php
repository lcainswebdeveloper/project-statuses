<?php

use App\Website;
use App\WebsiteRequest;
use Illuminate\Database\Seeder;

class AppsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('websites')->truncate();
        \DB::table('website_responses')->truncate();
        \DB::table('website_requests')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
        foreach([
            [
                'name' => 'API Gateway',
                'url' => 'http://v2.api.tshirtandsons.co.uk/web'
            ],
            [
                'name' => 'OMS',
                'url' => 'http://oms.tshirtandsons.co.uk/login'
            ],
            [
                'name' => 'API Media Server',
                'url' => 'https://master-media.tshirtandsons.co.uk/'
            ],
            [
                'name' => 'Customer Portal',
                'url' => 'https://portal.tshirtandsons.co.uk'
            ],
            [
                'name' => 'Shipping Service',
                'url' => 'https://shipping.tshirtandsons.co.uk'
            ],
            [
                'name' => 'SKU Manager',
                'url' => 'https://sku.tshirtandsons.co.uk/'
            ],
            [
                'name' => 'QX',
                'url' => 'https://tss-live.opendaqx.com/'
            ],
            [
                'name' => 'Endeavour',
                'url' => 'https://endeavour.tshirtandsons.co.uk'
            ],
            [
                'name' => 'TShirt & Sons',
                'url' => 'https://tshirtandsons.co.uk'
            ],
        ] as $website){
            $created = Website::create([
                'name' => $website['name']
            ]);
            WebsiteRequest::create([
                'website_id' => $created->id,
                'request_type' => 'GET',
                'url' => $website['url'],
            ]);
        }
    }
}
