<?php

namespace App\Http\Controllers\Admin;

use App\Website;
use App\WebsiteRequest;
use App\WebsiteResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class WebsiteRequestController extends Controller
{
    public function show(Website $website, WebsiteRequest $request){
        try{
            if($request->website_id != $website->id) throw new \Exception('That request does not belong to that website');
            $call = $request->guzzleCall();
            return response()->json([
              'status' => $call->getStatusCode()
            ]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    protected function validateRequest(Request $request){ 
        return $request->validate([
            'request_type' => [
                Rule::in(['GET', 'POST']),
            ],
            'url' => 'required|url',
            'request_headers' => 'nullable|json',
            'request_body' => 'nullable|json',
            'expected_status' => 'required|integer',
        ]);
    }

    public function store(Request $request, Website $website){
        $this->validateRequest($request);
        try{
            $website->requests()->create($request->all());
            $website->load('requests');
            return response()->json($website);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function update(Request $httpRequest, Website $website, WebsiteRequest $request){
        $this->validateRequest($httpRequest);
        try{
            if($request->website_id != $website->id) throw new \Exception('That request does not belong to that website');
            $request->update($httpRequest->all());
            $website->load('requests');
            return response()->json($website);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function refresh(Website $website, WebsiteRequest $request){
        try{
            if($request->website_id != $website->id) throw new \Exception('That request does not belong to that website');
            $request->sendRequest();
            $request->load('responses');
            return response()->json($request);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
