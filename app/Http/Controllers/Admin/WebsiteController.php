<?php

namespace App\Http\Controllers\Admin;

use App\Website;
use App\WebsiteResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Website::with('responses')->orderBy('name')->get();
        return view('admin.websites', [
            'websites' => Website::orderBy('name')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => [
                'required',
                'string',
                'unique:websites'
            ]
        ]);
        try{
            $website = new Website;
            $website->name = $request->name;
            $website->save();
            return response()->json([
                'added' => $website,
                'all' => Website::orderBy('name')->get()
            ]);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Website  $website
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Website $website)
    {
        $request->validate([
            'name' => [
                'required',
                'string',
                Rule::unique('websites')->ignore($website->id),
            ]
        ]);
        try{
            $website->name = $request->name;
            $website->save();
            return response()->json($website);
        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function show(Website $website) {
        $website->load('requests');
        
        return view('admin.website-requests', [
            'website' => $website
        ]);
    }
}
